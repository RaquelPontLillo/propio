package vista;

import controlador.Configurador;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VPrincipal {
    public static int menuInicial() {
        int opt;

        System.out.println("\n***************************");
        System.out.println("* MENÚ PRINCIPAL");
        System.out.println("* -------------------------");
        System.out.println("* 0. Salir");
        System.out.println("* -------------------------");
        System.out.println("* 1. Alumno");
        System.out.println("* 2. Curso");
        System.out.println("* 3. Matrícula");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerNumero();
        return opt;
    }
    
    public static char menuEstructura() {
        String nombre = Configurador.getNombreApp();
        char opt;
        
        System.out.println("\n" + nombre);
        System.out.println("\n********************************");
        System.out.println("* ESCOGE EL TIPO DE ESTRUCTURA");
        System.out.println("* ------------------------------");
        System.out.println("* s. Salir");
        System.out.println("* ------------------------------");
        System.out.println("* a. Array");
        System.out.println("* h. HashSet");
        System.out.println("********************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerCaracter();
        return opt;
    }
  
    public static String leerTexto() {
        Scanner scanner = new Scanner(System.in); 
        String texto = scanner.nextLine();
        return texto;
    }
    
    public static int leerNumero() {
        Scanner scanner = new Scanner(System.in); 
        int numero = scanner.nextInt();
        return numero;
    }
    
    public static char leerCaracter() {
        Scanner scanner = new Scanner(System.in);
        String cadena = scanner.next().toLowerCase();
        char caracter = cadena.charAt(0);
        return caracter;
    }
    
    public static void errorNum() {
        System.err.println("El valor introducido no es numérico. Por favor, introduce un número.");
    }
    
    public static boolean validarEmail(String s) {
        Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }
    
    public static void despedir() {
        System.out.println("\n¡Hasta luego! Cerrando aplicación...");
    }

    public static void errorMenu() {
        System.err.println("\nEscoge una de las opciones del menú.");
    }

    public static void despedirCRUD() {
        System.out.println("\nVolviendo al menú principal...");
    }
}