package controlador;

import java.util.HashSet;
import modelo.Alumno;
import modelo.IModelo;
import vista.IVista;
import vista.VAlumno;
import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CAlumno {
    private IModelo modelo;
    private IVista<Alumno> va = new VAlumno();
    private VAlumno vA = new VAlumno();
    
    public CAlumno(IModelo m) {
        modelo = m;
        String id;
        Boolean bucle = true;
        
        do {
            char opcion = VAlumno.menuCrud();
            Alumno alumno;

            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();
                    return;
                case 'c':
                    alumno = va.alta();
                    modelo.create(alumno);
                    break;
                case 'r':
                    HashSet hashset = new HashSet();
                    hashset = modelo.readAlumno();
                    vA.mostrarVarios(hashset);
                    break;
                case 'u':
                    id = vA.actualizar();
                    alumno = va.alta();
                    alumno.setId(id);
                    modelo.update(alumno);
                    break;
                case 'd':
                    id = vA.borrar();
                    alumno = new Alumno(id, "", "", 0, "");
                    modelo.delete(alumno);
                    break;
                default:
                    VPrincipal.errorMenu();
            }
        } while (bucle);
    }
}
